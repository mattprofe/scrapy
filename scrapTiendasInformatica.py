# Ejecutar con el siguiente comando:
# scrapy runspider scrapTiendasInformatica.py
# Luego cuando lo solicite escribir lo que desea buscar

import os
import sys
import time
import csv
import pandas as pd
from scrapy.item import Field
from scrapy.item import Item
from scrapy.spiders import Spider
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

# Interacción con el usuario
print("=======> Ingrese el nombre del producto")
valor = input()

# Nombre del archivo Csv resultado
nameCsv = "memoria_" + time.strftime("%d_%m_%y")

# Creacion de un Csv nuevo con los encabezados
def createCsv( name ):
    with open( name , mode='w') as arch_scrap:
                csv_scrap = csv.writer(arch_scrap, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_scrap.writerow(['web', 'dateTime', 'title', 'categ', 'url', 'price'])
                arch_scrap.close()

# Carga del Csv de forma acumulativa
def setCsv(name, web, dateTime, title, categ, url, price):
    with open( name , mode='a') as arch_scrap:
                csv_scrap = csv.writer(arch_scrap, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_scrap.writerow([web, dateTime, title, categ, url, price])

# Campos donde se guardan los datos de las arañas
class productos(Item):
    name = Field()
    price = Field()
    id = Field()
    category = Field()
    link = Field()
    fechaHora = Field()

# Araña de https://www.fullh4rd.com.ar
class scrapFullHard(Spider):
   
    fh = ""
    name = "fullhard"
    
    custom_settings = { 'DOWNLOAD_DELAY': 1 }
    
    # Web y busqueda del valor ingresado
    start_urls = ['https://www.fullh4rd.com.ar/cat/search/' + valor]

    def parse(self, response):

        sel = Selector(response)
 
        # Apuntamos al listado
        preguntas = sel.xpath('//div[@class="item product-list"]')

        # Recorremos el los items del listado
        for i, elem in enumerate(preguntas):

            item = ItemLoader(productos(), elem)

            # Apuntamos a un item del listado
            urlLink = 'https://www.fullh4rd.com.ar' + response.xpath('.//div[@class="item product-list"]/a/@href').extract()[i]
            titulo = response.xpath('.//h3/text()').extract()[i]
            precio = response.xpath('.//div[@class="price"]/text()').extract()[i]
            precio = precio.replace('$', '')

            self.fh = time.strftime("%d/%m/%y %H:%M:%S")

            # Envio de los datos obtenidos a la clase
            item.add_value('name', titulo)
            item.add_value('link', urlLink)
            item.add_value('price', precio)
            item.add_value('id', i)
            item.add_value('fechaHora', self.fh)

            # Carga de los datos en el Csv
            setCsv( nameCsv, self.name, self.fh, titulo, 'notFeature', urlLink, precio)
        
            yield item.load_item()


# Araña de https://www.maximus.com.ar
class scrapMaximus(Spider):

    item_count = 0
    fh = ""
    
    name = "maximus"

    custom_settings = { 'DOWNLOAD_DELAY': 1 }

    start_urls = ['https://www.maximus.com.ar/ARTICULOS/m=0/OR=0/BUS=' + valor + ';/maximus.aspx' ]
   
    rules = (
        Rule(LinkExtractor(allow=r'/A_PAGENUMBER='), callback = "parse", follow = True)
    )

    def parse(self, response):

        sel = Selector(response)

        preguntas = sel.xpath('//div[@class="row"]/div[@class="col-md-prod"]')

        for i, elem in enumerate(preguntas):

            item = ItemLoader(productos(), elem)
 
            titulo = response.xpath('.//a[@class="titprod"]/text()').extract()[i]
            urlLink = response.xpath('.//div[@class="vitrine-button"]/a/@href').extract()[i]
            precio = response.xpath('.//div[@class="price"]/text()').extract()[i]
            precio = precio.replace('ARS ', '')
           
            item.add_value('id', self.item_count)
            self.fh = time.strftime("%d/%m/%y %H:%M:%S")

            item.add_value('fechaHora', self.fh)
            item.add_value('name', titulo)
            item.add_value('price', precio)
            item.add_value('link', urlLink)
            
            self.item_count += 1
            
            setCsv( nameCsv, self.name, self.fh, titulo, 'notFeature', urlLink, precio)

            yield item.load_item()


# Araña de https://www.xt-pc.com.ar
class scrapXtPc(Spider):

    fh = ""

    name = "xtpc"
    start_urls = ['https://www.xt-pc.com.ar/cat/search/' + valor + '?' ]

    def parse(self, response):

        sel = Selector(response)
        
        preguntas = sel.xpath('//div[@class="white-area"]')

        for i, elem in enumerate(preguntas):

            item = ItemLoader(productos(), elem)

            titulo = preguntas.xpath('//div[@class="fullhard-product-mini-title"]/a/text()').extract()[i]
            price = preguntas.xpath('//div[@class="fullhard-product-mini-price"]/text()').extract()[i]
            precio = price.split('\n')[1].replace('$', '').replace(' ','')

            urlLink = 'https://www.xt-pc.com.ar' + preguntas.xpath('//div[@class="fullhard-product-mini-title"]/a/@href').extract()[i]

            item.add_value('name', titulo)
            item.add_value('link', urlLink)
            item.add_value('price', precio)
            item.add_value('id', i)

            self.fh = time.strftime("%d/%m/%y %H:%M:%S")
            item.add_value('fechaHora', self.fh)

            setCsv( nameCsv, self.name, self.fh, titulo, 'notFeature', urlLink, precio)

            yield item.load_item()

# Crea el csv con los encabezados para luego ordenarlo con pandas
createCsv(nameCsv)

# Ejecucion de las arañas
process = CrawlerProcess(get_project_settings())
process.crawl(scrapFullHard)
process.crawl(scrapMaximus)
process.crawl(scrapXtPc)
process.start()

# Ordenando por precio
listado = pd.read_csv( nameCsv )
# Ordenamos por precio descendente
listado = listado.sort_values(['price'], ascending=False)
# generamos un nuevo csv con el resultado ordenado
listado = listado.to_csv("ordenado_" + nameCsv)

# demora para detener

time.sleep(0.5)
os.execl(sys.executable, sys.executable, *sys.argv)